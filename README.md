ansible_role_mysql56
=========

Ansible Role for install mysql 5.6 and configure instances for RedHat 6/7

Requirements
------------

Ansible 2.5+
Python 2.7 or 3

Role Variables
--------------

- In defaults/main.yml add

ports:
  - 3307
  - 3308
  - 3309
  - 3310
  - ...
  
 
pass: 'p4ssw0rd'

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: mysql
      roles:
         - ansible_role_mysql56

Author Information
------------------

- Kevyn Perez kevynkl2@gmail.com
- +(502) 5412-7538
